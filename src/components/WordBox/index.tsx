import React from 'react';

import { Word as WordInterfaceProps } from '../../pages/Dashboard'

import { 
  WordContainer, 
  Word, 
  Language, 
  WordBoxContainer, 
  Id, 
  StyledButton 
} from './styles';

interface WordProps { 
  word?: string,
  language?: string,
  id?: number,
  deleteWord?: ((id: number) => void),
  updateWord?: (({id, word, language}: WordInterfaceProps) => void)
}

const WordBox: React.FC<WordProps> = ({
  word = '', 
  language = '', 
  id = NaN, 
  deleteWord = () => {}, 
  updateWord = () => {}
}: WordProps) => {

  return (
    <WordBoxContainer>
      <Id>{id}</Id>
      <WordContainer>
        <Word>{word}</Word>
        <Language>{language}</Language>
      </WordContainer>
      <StyledButton onClick={() => updateWord({id, word, language})}>Update</StyledButton>
      <StyledButton onClick={() => deleteWord(id)}>Delete</StyledButton>
    </WordBoxContainer>
  )
}

export default WordBox;