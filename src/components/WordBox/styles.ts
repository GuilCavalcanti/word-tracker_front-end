import styled from 'styled-components';
import { shade } from 'polished';

export const WordBoxContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-self: center;
  align-items: center;
  background: #232129;

  min-width: 250px;
  max-height: 740px;

  margin: 0px 20px 20px 0px;
  border: solid 2.5px #ff9000;
  border-radius: 10px;
`;

export const WordContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
`;

export const Word = styled.div`
  font-size: 25px;
`;

export const Language = styled.div`
  font-size: 25px;
  margin-top: 10px;
`;

export const Id = styled.div`
  display: flex;
  flex-direction: row;
  align-self: flex-start;

  margin: 3px 0px 0px 7px;
`;

export const StyledButton = styled.button`
  background: #ff9000;
  margin-top: 20px;
  height: 45px;
  width: 155px;
  border-radius: 10px;
  border: 0;
  padding: 0 16px;
  color: #312e38;
  font-weight: bold;
  font-size: 1.3rem;
  background: #ff9000;
  transition: background-color 0.2s;

  &:hover {
    background: ${shade(0.2, '#ff9000')};
  }

   font-family: 'Roboto Slab', serif;


  & + button {
    margin: 20px 0px 20px 0px;
  }
`;





