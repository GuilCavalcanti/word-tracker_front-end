import React, { useState, useEffect } from 'react';
import { StylesProvider, Snackbar } from '@material-ui/core'

import { Container, AlertStyled, AlertTitleStyled } from './styles';

interface ToastProps {
  showToast: boolean;
  toastMessageIndex?: number;
  toastStyleIndex?: number;
}

const ToastMessage: React.FC<ToastProps> = ({
  showToast = false,
  toastMessageIndex = 0
}: ToastProps) => {
  const [open, setOpen] = useState(false);
  const [skipFirstRender, setSkipFirstRender] = useState(1);
  const toastMessage = ["Word cannot be empty! Please enter a value.", 
  "Language cannot be empty! Please enter a value.", 
  "ID cannot be empty! Please enter a value.", 
  "There are no words stored. Add some words.",
  "There is no word with the given ID."];

  useEffect(() => {

    skipFirstRender === 1 
    ? setSkipFirstRender(2)
    : setOpen(true);
    
  }, [showToast]);

  return (
    <StylesProvider injectFirst>
      <Container>
        <Snackbar anchorOrigin={{ vertical: 'top', horizontal: 'right' }} open={open} autoHideDuration={3500} onClose={() => setOpen(false)}>
          <AlertStyled severity='error'>
            <AlertTitleStyled>Error</AlertTitleStyled>
            <strong>{toastMessage[toastMessageIndex]}</strong>
          </AlertStyled>
        </Snackbar>
      </Container>
    </StylesProvider>
  );
}

export default ToastMessage; 