import styled, { css } from 'styled-components';
import { Button } from '@material-ui/core';
import { shade } from 'polished';

interface UpdateWordContainerProps {
  isUpdateVisible?: boolean;
}

export const Container = styled.div`
  height: 100vh;
  display: flex;
  flex-direction: column;
`;

export const SearchContainer = styled.div`
  display: flex;
  flex-direction: row;
  
  background: #232129;

  width: fit-content;
  height: fit-content;

  margin-right:30px;
  padding: 15px;
  margin-left: 15px;

  border: solid 2.5px #ff9000;
  border-radius: 10px;

  button {
    background: #ff9000;
    margin-right: 5px;
    height: 50px;
    width: 145px;
    border-radius: 10px;
    border: 0;
    color: #312e38;
    font-weight: bold;
    font-size: 1.2rem;
    background: #ff9000;
    transition: background-color 0.2s;

    &:hover {
      background: ${shade(0.2, '#ff9000')};
    }

    font-family: 'Roboto Slab', serif;
  }

  button + button {
    width: 160px;
    margin-left: 10px;
  }
`;

export const IdSearchLabel = styled.div`
  align-self: center;
  font-size: 25px;
  margin-left: 5px;
`;

export const IdInputSearch = styled.input`
  width: 65px;
  background: transparent;
  border: solid 2.5px #ff9000;
  border-radius: 10px;
  padding: 5px;
  flex: 1;
  text-align: center; 

  margin-left: 10px;

  font-size: 25px;
  color: white;
`;

export const Title = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: center;

  h1 {
    margin-top: 20px;
    margin-bottom: 15px;
    font-size: 2.5rem;
    font-weight: 425; 
  }
`;

export const WordConfigContainer = styled.div`
  display: flex;
  flex-direction: column;
`;

export const WordOuterContainer = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
  padding: 15px;
`;

export const WordAddContainer = styled.div`
  display: flex;
  flex-direction: column;
  
  background: #232129;

  min-width: 455px;
  max-height: 310px;

  margin-right:30px;
  padding: 25px;

  border: solid 2.5px #ff9000;
  border-radius: 10px;
`;

export const UpdateWordContainer = styled.div<UpdateWordContainerProps>`
  display: flex;
  flex-direction: column;
  
  background: #232129;

  min-width: 455px;
  max-height: 310px;

  margin-right:30px;
  padding: 25px;

  border: solid 2.5px #ff9000;
  border-radius: 10px;

  margin-top: 25px;

  visibility: hidden;

  ${props => 
    props.isUpdateVisible &&
    css`
      & {
        visibility: visible;
      }
    `
  }
`;

export const WordAddInnerUpperContainer = styled.div`
  display: flex;
  flex-direction: row;
`;

export const LabelContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  margin-top: 22px;

  label {
    font-size: 25px;
  }

  label + label {
    margin-top: 40px;
  }
`;

export const InputContainer = styled.div`
  display: flex;
  flex-direction: column;
  
  align-items: center;
  color: #B8B8B8;
  font-size: 25px;

  & + div {
    margin-top: 10px;
  }
`;

export const WordAddInnerBottomContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
`;

export const Form = styled.form`
  width: 265px;

  margin-left: 30px;

  & + form {
    margin-top: 15px;
  }

  button {
    background: #ff9000;
    margin-top: 25px;
    margin-right: 5px;
    height: 65px;
    width: 175px;
    border-radius: 10px;
    border: 0;
    padding: 0 16px;
    color: #312e38;
    font-weight: bold;
    font-size: 1.3rem;
    background: #ff9000;
    transition: background-color 0.2s;

    &:hover {
      background: ${shade(0.2, '#ff9000')};
    }

    font-family: 'Roboto Slab', serif;
  }
`;

export const LanguageForm = styled.form`
  width: 265px;
  border: solid 2.5px #ff9000;
  border-radius: 10px;

  margin-left: 25px;
`;

export const Input = styled.input`
  width: 240px;
  background: transparent;
  border: solid 2.5px #ff9000;
  border-radius: 10px;
  padding: 10px;
  flex: 1;

  margin: 10px;

  font-size: 25px;
  color: white;
`;

export const StyledButton = styled(Button)`
  background: #ff9000;
  margin-top: 25px;
  margin-right: 500px;
  height: 65px;
  width: 175px;
  border-radius: 10px;
  border: 0;
  padding: 0 16px;
  color: #312e38;
  font-weight: bold;
  font-size: 1.3rem;
  background: #ff9000;
  transition: background-color 0.2s;

  &:hover {
    background: ${shade(0.2, '#ff9000')};
  }

   font-family: 'Roboto Slab', serif;
`;

export const WordsContainer = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  align-self: baseline;
`;



