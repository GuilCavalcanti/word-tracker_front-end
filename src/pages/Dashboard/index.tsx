import React, { useState, useEffect, FormEvent } from 'react';
import { StylesProvider } from '@material-ui/core';

import api from '../../services/api';
import WordBox from '../../components/WordBox';
import Toast from '../../components/Toast';
import { 
  Container, 
  Title, 
  WordOuterContainer, 
  WordAddContainer,
  WordAddInnerUpperContainer,
  WordAddInnerBottomContainer,
  LabelContainer,
  InputContainer, 
  Form,
  Input,
  IdInputSearch,
  UpdateWordContainer,
  WordsContainer,
  WordConfigContainer,
  SearchContainer,
  IdSearchLabel
} from './styles';

export interface Word {
  id: number;
  word: string;
  language: string;
}

const Dashboard: React.FC = () => {

  const [word, setWord] = useState('');
  const [updateWord, setUpdateWord] = useState({} as Word);
  const [language, setLanguage] = useState('');
  const [showToast, setShowToast] = useState(true);
  const [toastIndex, setToastIndex] = useState(0); 
  const [updateWordVisibility, setUpdateWordVisibility] = useState(false)
  const [searchById, setSearchById] = useState('');
  const [words, setWords] = useState<Word[]>([]);

  async function fetchData() {
    return await api.get<Word[]>('words');
  }

  useEffect(() => {
    async function fetchInitialData() {
      setWords((await fetchData()).data);
    }
    fetchInitialData();
  },[]);

  const handleAddWord = async (
    event: FormEvent<HTMLFormElement>
  ): Promise<void> => {
    event.preventDefault();

    if (word === "") {
      showToast ? setShowToast(false) : setShowToast(true);
      setToastIndex(0);
    } else if (language === "") {
      showToast ? setShowToast(false) : setShowToast(true);
      setToastIndex(1);
    } else {
      try {
        await api.post<Word>('words', {
            "word": word,
            "language": language
        });
        setWords((await fetchData()).data);
      }catch (Err) {
        console.log(Err);
      }
  
      setWord("");
      setLanguage("");
    }
  }

  const handleUpdateWord = async ({id, word, language}: Word) => {
    setUpdateWord({
      id,
      word,
      language
    });
    setUpdateWordVisibility(true);
  }

  const handleSubmitUpdateWord = async (event: FormEvent<HTMLFormElement>) => {
    event.preventDefault();

    if (updateWord.word === "") {
      showToast ? setShowToast(false) : setShowToast(true);
      setToastIndex(0);
    } else if (updateWord.language === "") {
      showToast ? setShowToast(false) : setShowToast(true);
      setToastIndex(1);
    } else {
      try {
        await api.patch<Word>('words/' + updateWord.id, {
            "word": updateWord.word,
            "language": updateWord.language
        });
        setWords((await fetchData()).data);
        setUpdateWordVisibility(false);
      }catch (Err) {
        console.log(Err);
      }
    }
  }

  const handleListAllWords = async () => {
    if (words.length === 0) {
      showToast ? setShowToast(false) : setShowToast(true);
      setToastIndex(3);
    } else {
      setWords((await fetchData()).data);
    }
  }

  const handleListWordById = async () => {
    if (searchById === '') {
      showToast ? setShowToast(false) : setShowToast(true);
      setToastIndex(2);
    } else if (words.length === 0){
      showToast ? setShowToast(false) : setShowToast(true);
      setToastIndex(3);
    } else {
      const { data } = await api.get<Word>('words/' + searchById)
      if (!data) {
        showToast ? setShowToast(false) : setShowToast(true);
        setToastIndex(4);
      } else {
        setWords([data])
      }
    }
  }

  const handleDeleteWord = async (id: number) => {

    try {
      await api.delete('words/' + id);
      setWords((await fetchData()).data);
    }catch (Err) {
      console.log(Err);
    }
  }

  return (
    <StylesProvider injectFirst>
      <Container>
        <Toast showToast={showToast} toastMessageIndex={toastIndex} />
        <Title>
        <h1>Word Tracker</h1>
        </Title>
        <SearchContainer>
          <button onClick={handleListAllWords}>List all words</button>
          <button onClick={handleListWordById}>List word by ID</button>
          <IdSearchLabel>ID</IdSearchLabel>
          <IdInputSearch value={searchById} name="searchById" onChange={(e) => setSearchById(e.target.value)}/>
        </SearchContainer>
        <WordOuterContainer>
          <WordConfigContainer>
            <WordAddContainer>
              <WordAddInnerUpperContainer>
                <LabelContainer>
                  <label>Word</label>
                  <label>Language</label>
                </LabelContainer>
                <InputContainer>
                  <Form onSubmit={handleAddWord}>
                    <Input value={word} name="word" onChange={(e) => setWord(e.target.value)} />
                    <Input value={language} name="language" onChange={(e) => setLanguage(e.target.value)} />
                    <button type="submit">Add Word</button>
                  </Form>
                </InputContainer>
              </WordAddInnerUpperContainer>
              <WordAddInnerBottomContainer>
              </WordAddInnerBottomContainer>
            </WordAddContainer>
            <UpdateWordContainer isUpdateVisible={updateWordVisibility}>
              <WordAddInnerUpperContainer>
                <LabelContainer>
                  <label>Word</label>
                  <label>Language</label>
                </LabelContainer>
                <InputContainer>
                  <Form onSubmit={handleSubmitUpdateWord}>
                    <Input value={updateWord.word} name="updateWord" onChange={(e) => setUpdateWord({...updateWord, word: e.target.value})}/>
                    <Input value={updateWord.language} name="updateLanguage" onChange={(e) => setUpdateWord({...updateWord, language: e.target.value})}/>
                    <button type="submit">Update Word</button>
                  </Form>
                </InputContainer>
              </WordAddInnerUpperContainer>
              <WordAddInnerBottomContainer>
              </WordAddInnerBottomContainer>
            </UpdateWordContainer>
          </WordConfigContainer>
          <WordsContainer>
            {words.map((wordObject: Word) => {
              if (wordObject !== undefined) {
                return <WordBox 
                          key={wordObject.id} 
                          id={wordObject.id} 
                          word={wordObject.word} 
                          language={wordObject.language}
                          deleteWord={handleDeleteWord}
                          updateWord={handleUpdateWord} 
                        />
              }
            })}
          </WordsContainer>
        </WordOuterContainer>
      </Container>
    </StylesProvider>
  )
}

export default Dashboard;